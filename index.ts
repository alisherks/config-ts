import * as appRootPath from 'app-root-path';
import * as ramda from 'ramda';

import { existsSync } from 'fs';
import { resolve } from 'path';

interface Irc {
    path?: string;
    extensions?: string[];
}

const rcPath = resolve(appRootPath.path, `.configrc.json`);
const rcExists = existsSync(rcPath);

let rc: Irc = {};

if (rcExists) {
    rc = require(rcPath);
}

const env = process.env.NODE_ENV || 'default';
const configExtensions = rc.extensions || ['ts', 'js'];
const configPath = configExtensions.map((ext) => resolve(appRootPath.path, rc.path || `config`, `${env}.${ext}`)).find((path: string) => existsSync(path));

let config = {};

if (configPath) {
    config = require(configPath).default;
}

Object.freeze(config);

export default {
    // tslint:disable-next-line:no-any
    get<T = any>(path: string) {
        return ramda.path(path.split('.'), config) as T;
    }
};
