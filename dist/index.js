"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var appRootPath = require("app-root-path");
var ramda = require("ramda");
var fs_1 = require("fs");
var path_1 = require("path");
var rcPath = path_1.resolve(appRootPath.path, ".configrc.json");
var rcExists = fs_1.existsSync(rcPath);
var rc = {};
if (rcExists) {
    rc = require(rcPath);
}
var env = process.env.NODE_ENV || 'default';
var configExtensions = rc.extensions || ['ts', 'js'];
var configPath = configExtensions.map(function (ext) { return path_1.resolve(appRootPath.path, rc.path || "config", env + "." + ext); }).find(function (path) { return fs_1.existsSync(path); });
var config = {};
if (configPath) {
    config = require(configPath).default;
}
Object.freeze(config);
exports.default = {
    // tslint:disable-next-line:no-any
    get: function (path) {
        return ramda.path(path.split('.'), config);
    }
};
