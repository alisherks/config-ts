declare const _default: {
    get<T = any>(path: string): T;
};
export default _default;
